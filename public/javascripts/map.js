var mymap = L.map('main_map').setView([-31.413223, -64.190553], 12);
var marker = L.marker([-31.413223, -64.190553]).addTo(mymap);
marker.bindPopup("<b>Centro Córdoba Capital, Argentina</b><br>-31.413223, -64.190553").openPopup();
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibmlhY2VsbyIsImEiOiJja2hucHpwNmEwM3AyMnpwMG1lNWtnODJ3In0._ohSLyRgBpwp6yorm50ltQ'
}).addTo(mymap);

$.ajax({
	dataType: 'json',
	url: 'api/bicicletas',
	success: function(result) {
		console.log(result);
		result.bicicletas.forEach(function(bici){
			L.marker(bici.ubicacion, {title: bici.id + ' - ' + bici.color}).addTo(mymap);
		});
	}
})