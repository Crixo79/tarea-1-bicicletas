# Tarea N°1: Bicicletas
## Tabla de contenidos
1. [Instrucciones](#instrucciones)
2. [Proyecto Previo](#proyecto_previo)
3. [Tarea a realizar](#tarea_a_realizar)
4. [Atribuciones](#atribuciones)
5. [Licencia](#licencia)

### Instrucciones
***
```bash
git clone https://Crixo79@bitbucket.org/Crixo79/tarea-1-bicicletas.git
npm install
nodemon devstart
```

### [Proyecto Previo](https://bitbucket.org/Crixo79/red_bicicletas)
***

### Tarea a realizar
***

##### 1. Un archivo README (sólo texto) en el repositorio de Bitbucket.

##### 2. El mensaje de bienvenida a Express.

##### 3. El proyecto vinculado con el repositorio de Bitbucket creado previamente.

##### 4. El servidor que se ve correctamente, comparándolo con la versión original.

##### 5. Un mapa centrado en una ciudad.

##### 6. Marcadores indicando una  ubicación.

##### 7. Un script npm en el archivo package.json, debajo del “start”, que se llama “devstart” y que levanta el servidor utilizando nodemon.

##### 8. Un par de bicicletas en la colección que conoce la bicicleta en memoria, con las ubicaciones cercanas al centro del mapa agregado.

##### 9. Una colección del modelo de bicicleta.

##### 10. Los endpoints de la API funcionando con Postman.

***
#### Realizado por Eduardo Nicolás Planel
#### Para el curso de la Universidad Austral en NODEJS
***

## Licencia
[MIT](https://choosealicense.com/licenses/mit/)